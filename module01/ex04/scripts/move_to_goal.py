#!/usr/bin/env python
import rospy
import sys
from geometry_msgs.msg  import Twist
from turtlesim.msg import Pose
from math import pow,atan2,sqrt

class ex04():

    def __init__(self):
        #Creating our node,publisher and subscriber
        rospy.init_node('move_to_goal', anonymous=True)
        self.velocity_publisher = rospy.Publisher('/turtle1/cmd_vel', Twist, queue_size=10)
        self.pose_subscriber = rospy.Subscriber('/turtle1/pose', Pose, self.callback)
        self.pose = Pose()
        self.rate = rospy.Rate(10)

    #Callback function implementing the pose value received
    def callback(self, data):
        self.pose = data
        self.pose.x = round(self.pose.x, 8)
        self.pose.y = round(self.pose.y, 8)

    def get_distance(self, goal_x, goal_y):
        distance = sqrt(pow((goal_x - self.pose.x), 2) + pow((goal_y - self.pose.y), 2))
        return distance
    def turtle_goal(self,x_d,y_d,theta_d):
        distance_tolerance = 0.000001
        angle_tolerance = 0.000001
        vel_msg = Twist()

        #from deg2rad
        # theta_d1 =theta_d
        theta_d1 =theta_d*3.1415926535/180


        while sqrt(pow((x_d - self.pose.x), 2) + pow((y_d - self.pose.y), 2)) >= distance_tolerance:
            print("x: "+str(self.pose.x)+" y: "+str(self.pose.y) + " theta: "+str(self.pose.theta))
            #Porportional Controller
            #linear velocity
            vel_msg.linear.x = 3 * sqrt(pow((x_d - self.pose.x), 2) + pow((y_d - self.pose.y), 2))
            vel_msg.linear.y = 0
            vel_msg.linear.z = 0
            #angular velocity:
            vel_msg.angular.x = 0
            vel_msg.angular.y = 0
            vel_msg.angular.z = 15* (atan2(y_d - self.pose.y, x_d - self.pose.x) - self.pose.theta)
            #Publishing our vel_msg
            self.velocity_publisher.publish(vel_msg)
            self.rate.sleep()
        #Stopping our robot after the movement is over
        vel_msg.linear.x = 0
        vel_msg.linear.y = 0

        vel_msg.angular.z = 0

        print("done")
        rospy.spin()

if __name__ == '__main__':
    try:
        #Testing our function
        x = ex04()
        x.turtle_goal(float(sys.argv[1]), float(sys.argv[2]), float(sys.argv[3]))

    except rospy.ROSInterruptException: pass
