#!/usr/bin/env python
import rospy
from ex00.srv import service_name, service_nameResponse
def handle_fullname(req):
    print("Returning [%s + %s + %s = %s]"%(req.name1, req.name2, req.name3, (req.name1 +" "+ req.name2 +" "+ req.name3)))
    return service_nameResponse(req.name1 +" "+ req.name2 +" " +req.name3)

def server_work():
    rospy.init_node('vik')
    service = rospy.Service("summ_full_name",service_name, handle_fullname)
    print("Serv is Ok")
    rospy.spin()

if __name__ == "__main__":
    server_work()
