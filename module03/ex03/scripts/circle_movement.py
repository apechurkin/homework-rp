#!/usr/bin/env python
import rospy
import sys
from geometry_msgs.msg  import Twist
import time
def rotation_gazebo():
        #Creating our node,publisher and subscriber
        rospy.init_node('rotate_gazebo', anonymous=True)
        velocity_publisher = rospy.Publisher('/cmd_vel', Twist, queue_size=10)
        rate = rospy.Rate(10)
        vel_msg = Twist()
        while not rospy.is_shutdown():
             vel_msg.linear.x = 0.5
             vel_msg.linear.y = 0
             vel_msg.linear.z = 0
             vel_msg.angular.x = 0
             vel_msg.angular.y = 0
             vel_msg.angular.z = 0.8
            #Publishing our vel_msg
             velocity_publisher.publish(vel_msg)
             rate.sleep()

if __name__ == '__main__':
    try:
        print("Start")
        #Testing our function
        
        rotation_gazebo()
    except rospy.ROSInterruptException: pass
