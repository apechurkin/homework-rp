#!/usr/bin/env python
import rospy
from geometry_msgs.msg import Twist
import sys

def robot_move(message):
    rospy.init_node('move_turtle', anonymous=True)
    pub = rospy.Publisher('/turtle1/cmd_vel', Twist, queue_size=10)
    rate = rospy.Rate(10)  # 10
    vel = Twist()
    if message == "turn_left":
        while not rospy.is_shutdown():
            vel.linear.x = 0.0
            vel.linear.y = 0
            vel.linear.z = 0
            vel.angular.x = 0
            vel.angular.y = 0
            vel.angular.z = 1.0
            rospy.loginfo("Linear Vel = %f: Angular Vel = %f", vel.linear.x, vel.angular.z)
            pub.publish(vel)
            rate.sleep()
    elif message == "turn_right":
        while not rospy.is_shutdown():
            vel.linear.x = 0.0
            vel.linear.y = 0
            vel.linear.z = 0
            vel.angular.x = 0
            vel.angular.y = 0
            vel.angular.z = -1.0
            rospy.loginfo("Linear Vel = %f: Angular Vel = %f", vel.linear.x, vel.angular.z)
            pub.publish(vel)
            rate.sleep()
    elif message == "move_forward":
        while not rospy.is_shutdown():
            vel.linear.x = 1.0
            vel.linear.y = 0
            vel.linear.z = 0
            vel.angular.x = 0
            vel.angular.y = 0
            vel.angular.z = 0
            rospy.loginfo("Linear Vel = %f: Angular Vel = %f", vel.linear.x, vel.angular.z)
            pub.publish(vel)
            rate.sleep()
    elif message == "move_backward":
        while not rospy.is_shutdown():
            vel.linear.x = -1.0
            vel.linear.y = 0
            vel.linear.z = 0
            vel.angular.x = 0
            vel.angular.y = 0
            vel.angular.z = 0
            rospy.loginfo("Linear Vel = %f: Angular Vel = %f", vel.linear.x, vel.angular.z)
            pub.publish(vel)
            rate.sleep()

if __name__ == '__main__':
    try:
        robot_move(str(sys.argv[1]))
    except rospy.ROSInterruptException:
        pass
#
